package com.example.parwez.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class Basiccalculation extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basiccalculation);
    }
    public void onclickplus(View view)
    {
        EditText e1 = (EditText) findViewById(R.id.editText);
        EditText e2 = (EditText) findViewById(R.id.editText2);
        TextView  t = (TextView) findViewById(R.id.textView);
        float a =  Float.valueOf(e1.getText().toString());
        float b =  Float.valueOf(e2.getText().toString());
        float  c =  a + b ;
        t.setText(Float.toString(c));
    }
    public void onclickminus(View view)
    {
        EditText e1 = (EditText) findViewById(R.id.editText);
        EditText e2 = (EditText) findViewById(R.id.editText2);
        TextView  t = (TextView) findViewById(R.id.textView);
        float a =  Float.valueOf(e1.getText().toString());
        float b =  Float.valueOf(e2.getText().toString());
        float  c =  a - b ;
        t.setText(Float.toString(c));
    }
    public void onclickmult(View view)
    {
        EditText e1 = (EditText) findViewById(R.id.editText);
        EditText e2 = (EditText) findViewById(R.id.editText2);
        TextView  t = (TextView) findViewById(R.id.textView);
        float a =  Float.valueOf(e1.getText().toString());
        float b =  Float.valueOf(e2.getText().toString());
        float  c =  a * b ;
        t.setText(Float.toString(c));
    }
    public void onclickdiv(View view)
    {
        EditText e1 = (EditText) findViewById(R.id.editText);
        EditText e2 = (EditText) findViewById(R.id.editText2);
        TextView  t = (TextView) findViewById(R.id.textView);
        float a =  Float.valueOf(e1.getText().toString());
        float b =  Float.valueOf(e2.getText().toString());
        float  c =  a / b ;
        t.setText(Float.toString(c));
    }
    public void onclickremain(View view)
    {
        EditText e1 = (EditText) findViewById(R.id.editText);
        EditText e2 = (EditText) findViewById(R.id.editText2);
        TextView  t = (TextView) findViewById(R.id.textView);
        float a =  Float.valueOf(e1.getText().toString());
        float b =  Float.valueOf(e2.getText().toString());
        int   c = (int) a;
        int d = (int) b;
        int e  =  c % d ;
        t.setText(Integer.toString(e));
    }
    public void onclickexponent(View view)
    {
        EditText e1 = (EditText) findViewById(R.id.editText);
        EditText e2 = (EditText) findViewById(R.id.editText2);
        TextView  t = (TextView) findViewById(R.id.textView);
        float a =  Float.valueOf(e1.getText().toString());
        float b =  Float.valueOf(e2.getText().toString());
        double   d =  Math.pow( a,b);
        t.setText(Float.toString((float) d));
    }
}
